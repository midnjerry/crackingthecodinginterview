package balderas.jerry.twitter.hackingTime;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SolutionTest {
	private Solution solution;

	@Before
	public void setUp() {
		solution = new Solution();
	}
	
	/*
	 * Given a Tuple for eg. (a, b, c).. 
     * Output : (*, *, *), (*, *, c), (*, b, *), (*, b, c), (a, *, *), (a, *, c), (a, b, *), (a, b, c)
	 */
	@Test
	public void printTuple() {
		String input = "(a,b,c)";
		String expected = "(*, *, *), (*, *, c), (*, b, *), (*, b, c), (a, *, *), (a, *, c), (a, b, *), (a, b, c)";
		assertEquals(expected, solution.printTuple(input));
		
		input = "()";
		expected = "()";
		assertEquals(expected, solution.printTuple(input));
		
		input = "(d)";
		expected = "(*), (d)";
		assertEquals(expected, solution.printTuple(input));		
	}
	
	@Test
	public void passesSample() {
		String input = "Hi, this is an example";
		String numericKey = "4071321"; 		
		String result = solution.encrypt(input, numericKey);
		assertEquals("Li, ailu jw au facntll", result);
		assertEquals(input, solution.decrypt(result, numericKey));
	}
	
	@Test
	public void passesFindKey() {
		String encrypted = "Atvt hrqgse, Cnikg";
		String decoded = "Your friend, Alice";		 		
		String numericKey = solution.calcKey(decoded, encrypted);
		System.out.println("Alice's calculated key: " + numericKey);
		assertEquals(encrypted, solution.encrypt(decoded, numericKey));		
	}
	
	@Test
	public void solveProblem(){
		String input = "Otjfvknou kskgnl, K mbxg iurtsvcnb ksgq hoz atv. Vje xcxtyqrl vt ujg smewfv vrmcxvtg rwqr ju vhm ytsf elwepuqyez. -Atvt hrqgse, Cnikg"; 
		String numericKey = "8251220";
		String result = solution.decrypt(input, numericKey);
		System.out.println("Alice Typed: " + result);		
	}
	
	
	@Test 
	public void rollLetterTests(){
		assertEquals('D',solution.rollLetter('A', 3));
		assertEquals('A',solution.rollLetter('A', 0));
		assertEquals('A',solution.rollLetter('X', 3));
		assertEquals('d',solution.rollLetter('a', 3));
		assertEquals('a',solution.rollLetter('a', 0));
		assertEquals('a',solution.rollLetter('x', 3));
	}
	
	
}
