package balderas.jerry.crackingTheCodingInterview.chapter1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Solution1_3_Test {
	private Solution1_3 solution;

	@Before
	public void setUp() {
		solution = new Solution1_3();
	}

	private void testInput(String input, String expected) {
		assertEquals(expected, solution.removeDuplicateCharacters(input));
	}

	@Test
	public void removeDuplicateCharacters_returnsNull_onNullInput() {
		assertNull(solution.removeDuplicateCharacters(null));
	}

	@Test
	public void removeDuplicateCharacters_input_expected() {
		testInput("A", "A");
		testInput("AA", "A");
		testInput("ABCAA", "ABC");
		testInput("aBCAA", "aBC");
		testInput("aaAAaaAAbbBBbbBB", "ab");
		testInput("aaAAaaAAbbBBbbBBccCCccCCccCCccCC", "abc");
		testInput("abcaAAaaAAbbBBbbBBccCCccCCccCCccCC", "abc");
	}
}
