package balderas.jerry.crackingTheCodingInterview.chapter1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Solution1_1_Test {
	private Solution1_1 evaluator;

	@Before
	public void setUp() {
		evaluator = new Solution1_1();
	}

	private void testInputString(String string, boolean result) {
		assertEquals(result, evaluator.areLettersUnique(string));
		assertEquals(result, evaluator.areLettersUniqueNoExtraDataStructures(string));
	}
	
	@Test
	public void returnsTrueOnEmptyString() {
		testInputString("", true);
	}
	
	@Test (expected = NullPointerException.class)
	public void returnsExceptionOnNull() {
		 evaluator.areLettersUnique(null);	
	}
	
	@Test (expected = NullPointerException.class)
	public void solutionNoExtraDataStructuresReturnsExceptionOnNull() {
		 evaluator.areLettersUniqueNoExtraDataStructures(null);	
	}
	
	@Test
	public void returnsFalseOnSameLetters() {
		testInputString("aa", false);		
	}
	
	@Test
	public void returnsTrueOnOneLetter() {
		testInputString("a", true);	
	}
	
	@Test
	public void returnsFalseOnLowerAndUpper() {
		testInputString("Aa", false);		
	}
	
	@Test
	public void returnsTrueAlphabetandNumbers() {
		testInputString("abcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()_+", true);		
	}
	
	@Test
	public void returnsTrueIfSpecialCharactersSame() {
		testInputString("**", true);		
	}
	
	@Test
	public void returnsTrueIfNumbersAreTheSame() {
		testInputString("11A11", true);		
	}
	
}
