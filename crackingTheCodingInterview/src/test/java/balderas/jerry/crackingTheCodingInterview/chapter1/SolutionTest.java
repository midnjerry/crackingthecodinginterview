package balderas.jerry.crackingTheCodingInterview.chapter1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SolutionTest {
	private Solution solution;

	@Before
	public void setUp() {
		solution = new Solution();
	}

	@Test
	public void question_1_2_isPermutationOfOther() {
		assertEquals(true, solution.isPermutationOf("abc", "bca"));
		assertEquals(false, solution.isPermutationOf("abc", "bcs"));
	}

	@Test
	public void question_1_3_urlify() {
		assertEquals("the%20house%20is%20near", solution.URLify("the house is near"));
		assertEquals("the%20%20rock", solution.URLify("the  rock"));
		assertEquals("therock", solution.URLify("therock"));
	}

	@Test
	public void question_1_4_Palindrome_Permutation() {
		assertEquals(false, solution.isPalindrome(""));
		assertEquals(true, solution.isPalindrome("a"));
		assertEquals(false, solution.isPalindrome("cat"));
		assertEquals(true, solution.isPalindrome("aaaa"));
		assertEquals(true, solution.isPalindrome("Tact Coa"));
		assertEquals(true, solution.isPalindrome("tact coa"));
		assertEquals(false, solution.isPalindrome("tact cob"));
		assertEquals(true, solution.isPalindrome("catss cat"));
		assertEquals(true, solution.isPalindrome("catrrr  cat"));
		assertEquals(true, solution.isPalindrome("catrr catr"));
		assertEquals(false, solution.isPalindrome("catab cat"));
		assertEquals(true, solution.isPalindrome("cata cat"));
	}

	@Test
	public void question_1_5_isOneChangeAway() {
		assertEquals(true, solution.isOneChangeAway("", ""));
		assertEquals(true, solution.isOneChangeAway("pale", "ple"));
		assertEquals(true, solution.isOneChangeAway("pales", "pale"));
		assertEquals(true, solution.isOneChangeAway("pale", "bale"));
		assertEquals(false, solution.isOneChangeAway("pale", "bake"));
		assertEquals(false, solution.isOneChangeAway("a", "aaa"));
	}
	
	@Test
	public void question_1_6_StringCompression() {
		assertEquals("a2b1c5a3", solution.compress("aabcccccaaa"));
		assertEquals("", solution.compress(""));
		assertEquals("abc", solution.compress("abc"));
		assertEquals("aabbcc", solution.compress("aabbcc"));
		assertEquals("a2b2c3", solution.compress("aabbccc"));
		assertEquals("A2b2c3", solution.compress("AAbbccc"));
		assertEquals("A1a5A6b1c1", solution.compress("AaaaaaAAAAAAbc"));		
		assertEquals("A1a5A6b3c1", solution.compress("AaaaaaAAAAAAbbbc"));
	}

}
