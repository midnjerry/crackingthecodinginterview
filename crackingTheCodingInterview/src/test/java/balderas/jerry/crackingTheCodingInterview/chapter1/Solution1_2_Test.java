package balderas.jerry.crackingTheCodingInterview.chapter1;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class Solution1_2_Test {
	private Solution1_2 solution;

	@Before
	public void setUp() {
		solution = new Solution1_2();
	}

	private boolean doesCStringMatch(Character[] result, String input) {
		if (result.length != input.length() + 1) {
			return false;
		}

		for (int i = 0; i < input.length(); i++) {
			if (result[i] != input.charAt(i)) {
				return false;
			}
		}

		if (result[input.length()] != '\0') {
			return false;
		}

		return true;
	}

	@Test
	public void getCStringReturnsNullOnNullInput() {
		assertNull(null, solution.getCString(null));
	}

	@Test
	public void getCStringReturnsArrayOfOneWithNullCharacter() {
		String input = "";
		Character[] result = solution.getCString(input);
		assertEquals(input.length() + 1, result.length);
		assertEquals(true, doesCStringMatch(result, input));
	}

	@Test
	public void getCStringReturnsArrayOfABCWithNullCharacter() {
		String input = "ABC";
		Character[] result = solution.getCString(input);
		assertEquals(input.length() + 1, result.length);
		assertEquals(true, doesCStringMatch(result, input));
	}

	@Test
	public void reverseCStringreturnsNullOnNullInput() {
		String input = null;
		assertNull(null, solution.reverseCString(solution.getCString(input)));
	}

	@Test
	public void reverseCStringReturnsCBANull_on_ABCNull() {
		String string = "ABC";
		Character[] input = solution.getCString(string);
		Character[] result = solution.reverseCString(input);		
		assertEquals(true, doesCStringMatch(result,"CBA"));
  	}

}
