package balderas.jerry.crackingTheCodingInterview.chapter2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedListTest;

public class Solution2_2_Test {
	private Solution2_2 list;

	@Before
	public void setUp() {
		list = new Solution2_2();
	}

	@Test
	public void returnsTrueIfNumbersAreTheSame() {
		LinkedListTest.add(list, 1, 2, 3, 4, 5, 6, 7, 8);
		assertEquals((Integer)6, list.getNthToLastElement(2));		
	}
	
	@Test
	public void NthItemIsGreaterThanList_ReturnsNull() {
		LinkedListTest.add(list, 1, 2);
		assertNull( list.getNthToLastElement(3));		
	}	
}
