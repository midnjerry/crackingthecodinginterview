package balderas.jerry.crackingTheCodingInterview.chapter2;

import org.junit.Before;
import org.junit.Test;

import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedListTest;
import balderas.jerry.crackingTheCodingInterview.dataStructures.Node;

public class Solution2_3_Test {
	private Solution2_3 list;

	@Before
	public void setUp() {
		list = new Solution2_3();
	}

	@Test
	public void deleteTwo() {
		LinkedListTest.add(list, 1, 2, 3);
		Node n = list.head.next;
		list.deleteNode(n);
		LinkedListTest.verify(list, 1, 3);
	}

	@Test
	public void deleteTail() {
		LinkedListTest.add(list, 1, 2);
		Node n = list.head.next;
		list.deleteNode(n);
		LinkedListTest.verify(list, 1);
	}

	@Test
	public void deleteHead() {
		LinkedListTest.add(list, 1, 2);
		Node n = list.head;
		list.deleteNode(n);
		LinkedListTest.verify(list, 2);
	}

	@Test
	public void ignoreNodeThatDoesntExist() {
		LinkedListTest.add(list, 1, 2, 3);
		Node n = new Node(4);
		list.deleteNode(n);
		LinkedListTest.verify(list, 1, 2, 3);
	}

	@Test
	public void ignoreDeleteNodeEmptyList() {
		list.deleteNode(new Node(3));
	}
}
