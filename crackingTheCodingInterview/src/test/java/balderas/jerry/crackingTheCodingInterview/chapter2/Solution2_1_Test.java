package balderas.jerry.crackingTheCodingInterview.chapter2;

import org.junit.Before;
import org.junit.Test;

import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedListTest;

public class Solution2_1_Test {
	private Solution2_1 list;

	@Before
	public void setUp() {
		list = new Solution2_1();
	}

	@Test
	public void returnsTrueIfNumbersAreTheSame() {
		LinkedListTest.add(list, 5, 6, 7, 5, 6, 7, 5, 5);
		list.removeDuplicates();
		LinkedListTest.verify(list, 5, 6, 7);
	}

	@Test
	public void removesSimpleDuplicate() {
		LinkedListTest.add(list, 5, 5);
		list.removeDuplicates();
		LinkedListTest.verify(list, 5);
	}

	@Test
	public void removesLongDuplicateChain() {
		LinkedListTest.add(list, 5, 5, 5, 5, 5, 5, 5, 5, 6, 5, 5, 5, 7, 5, 5, 5, 5, 5, 7);
		list.removeDuplicates();
		LinkedListTest.verify(list, 5, 6, 7);
	}

	@Test
	public void emptyListExecutes() {
		list.removeDuplicates();
	}

	@Test
	public void removeDuplicates2_emptyListExecutes() {
		list.removeDuplicates2();
	}

	@Test
	public void removeDuplicates2_removeLongChain() {
		LinkedListTest.add(list, 5, 5, 5, 5, 5, 5, 5, 5, 6, 5, 5, 5, 7, 5, 5, 5, 5, 5, 7);
		list.removeDuplicates2();
		LinkedListTest.verify(list, 5, 6, 7);
	}

	@Test
	public void removeDuplicates2_removesSimpleDuplicate() {
		LinkedListTest.add(list, 5, 5);
		list.removeDuplicates2();
		LinkedListTest.verify(list, 5);
	}
}
