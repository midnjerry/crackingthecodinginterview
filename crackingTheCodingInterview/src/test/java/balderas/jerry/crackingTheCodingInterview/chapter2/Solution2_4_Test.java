package balderas.jerry.crackingTheCodingInterview.chapter2;

import org.junit.Before;
import org.junit.Test;

import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedList;
import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedListTest;

public class Solution2_4_Test {
	private Solution2_4 solution;
	private LinkedList list;
	private LinkedList list2;
	private LinkedList result;

	@Before
	public void setUp() {
		solution = new Solution2_4();

		list = new LinkedList();
		list2 = new LinkedList();
		result = new LinkedList();
	}

	@Test
	public void ignoreDeleteNodeEmptyList() {
		LinkedListTest.add(list, 0, 0, 0);
		LinkedListTest.add(list2, 0, 0);
		result = solution.addOperation(list, list2);
		LinkedListTest.verify(result, 0, 0, 0);
	}
	
	@Test
	public void simpleAddition() {
		LinkedListTest.add(list, 1, 0, 0);
		LinkedListTest.add(list2, 1, 1);
		result = solution.addOperation(list, list2);
		LinkedListTest.verify(result, 2, 1, 0);
	}
	
	@Test
	public void additionWithCarryover() {
		LinkedListTest.add(list, 6, 0, 0);
		LinkedListTest.add(list2, 5, 1);
		result = solution.addOperation(list, list2);
		LinkedListTest.verify(result, 1, 2, 0);
	}
	
	@Test
	public void threeDigitAddition() {
		LinkedListTest.add(list, 3, 1, 5);
		LinkedListTest.add(list2, 5, 9, 2);
		result = solution.addOperation(list, list2);
		LinkedListTest.verify(result, 8, 0, 8);
	}
	
	@Test
	public void conversionOfCarryOver10() {
		LinkedListTest.add(list, 123, 0, 0);
		LinkedListTest.add(list2, 0, 0, 0);
		result = solution.addOperation(list, list2);
		LinkedListTest.verify(result, 3, 2, 1);
	}
	
	@Test
	public void conversionOfCarryOver100() {
		LinkedListTest.add(list, 123, 25, 5);
		LinkedListTest.add(list2, 454, 64, 6);
		result = solution.addOperation(list, list2);
		LinkedListTest.verify(result, 7, 6, 5, 2);
	}
}
