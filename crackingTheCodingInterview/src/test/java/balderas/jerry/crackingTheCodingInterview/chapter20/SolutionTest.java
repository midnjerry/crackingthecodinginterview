package balderas.jerry.crackingTheCodingInterview.chapter20;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedListTest;

public class SolutionTest {
	private Solution solution;

	@Before
	public void setUp() {
		solution = new Solution();
	}

	/*
	 * Write a function that adds two numbers. You should not use + or any arithmetic operators.
	 */
	@Test
	public void addOperatorWorks() {
		assertEquals(5, solution.add(4, 1));
		assertEquals(3, solution.add(2, 1));
		assertEquals(2, solution.add(1, 1));
		assertEquals(10, solution.add(5, 5));
		assertEquals(20, solution.add(10, 10));
		assertEquals(6, solution.add(3, 3));
		assertEquals(45, solution.add(30, 15));
		assertEquals(15, solution.add(30, -15));
		assertEquals(-10, solution.add(-5, -5));
	}

	/*
	 * Write a method to shuffle a deck of cards. It must be a perfect shuffle - in other words,
     * each 52! permutations of the deck has to be equally likely. Assume that you are given
     * a random number generator which is perfect.
	 */
	@Test
	public void shuffleDeck() {
		Deck deck = new Deck();
		deck.shuffleDeck();
		while (deck.getSize() > 0) {
			System.out.print(deck.drawCard() + " ");
		}
		System.out.println();
	}
	
	/*
	 * Write a method to randomly generate a set of m integers from an array of size n. Each
	 * element must have equal probability of being chosen.  Each element should only be
	 * selected twice.  Do not use REMOVE for array.
	 */
	@Test
	public void generateSet(){
		int[] input = {10, 20, 30, 40, 50, 60, 70};
		int[] set = solution.generateSet(input, 3);
		System.out.println(Arrays.toString(set));
		
		set = solution.generateSet(input, 7);
		System.out.println(Arrays.toString(set));
	}
	
	/*
	 * Write a method to count the number of 2s between 0 and n.
	 */
	@Test
	public void countTwos(){
		assertEquals(solution.bookSolution(0), solution.countTwos(0));
		assertEquals(solution.bookSolution(10), solution.countTwos(10));
		assertEquals(solution.bookSolution(2), solution.countTwos(2));
		assertEquals(solution.bookSolution(12), solution.countTwos(12));
		assertEquals(solution.bookSolution(19), solution.countTwos(19));
		assertEquals(solution.bookSolution(20), solution.countTwos(20));
		assertEquals(solution.bookSolution(62), solution.countTwos(62));
		assertEquals(solution.bookSolution(3000), solution.countTwos(3000));
		assertEquals(solution.bookSolution(10000), solution.countTwos(10000));
	}
	
	
}
