package balderas.jerry.crackingTheCodingInterview.dataStructures;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;

public class BinaryTreeTest {
	private BinaryTree tree;

	@Before
	public void setUp() {
		tree = new BinaryTree();
	}

	public static void add(BinaryTree tree, int... numbers) {
		for (int num : numbers) {
			tree.insertNode(num);
		}
	}

	public static void verify(ArrayList<Integer> list, int... numbers) {
		assertEquals(numbers.length, list.size());

		for (int i = 0; i < numbers.length; i++) {
			assertEquals(numbers[i], (int) list.get(i));			
		}
	}

	@Test
	public void addEntriesAndVerifyOrder() {
		int[] numbers = { 5, 10, 15, 20, 3, 7, 21, 45, 30 };
		add(tree, numbers);
		Arrays.sort(numbers);
		ArrayList<Integer> list = tree.inOrder();
		verify(list, numbers);
	}
	
	@Test
	public void verifyPreOrderTraversal() {
		int[] numbers = { 20, 10, 30, 5, 15, 25, 35 };
		add(tree, numbers);
	
		ArrayList<Integer> list = tree.preOrder();
		System.out.println("PreOrder:"+ Arrays.toString(list.toArray()));
		verify(list, 20, 10, 5, 15, 30, 25, 35);
	}
	
	@Test
	public void verifyPostOrderTraversal() {
		int[] numbers = { 20, 10, 30, 5, 15, 25, 35 };
		add(tree, numbers);
	
		ArrayList<Integer> list = tree.postOrder();
		System.out.println("PostOrder:"+ Arrays.toString(list.toArray()));
		verify(list, 5, 15, 10, 25, 35, 30, 20);
	}
	
	@Test
	public void verifyPostOrderTraversalSorted() {
		int[] numbers = { 1, 2, 3, 4, 5, 6, 7};
		add(tree, numbers);
	
		ArrayList<Integer> list = tree.postOrder();
		System.out.println("PostOrder:"+ Arrays.toString(list.toArray()));
		verify(list, 7, 6, 5, 4, 3, 2, 1);
	}
	
	@Test
	public void verifyPreOrderTraversalSorted() {
		int[] numbers = { 1, 2, 3, 4, 5, 6, 7};
		add(tree, numbers);
	
		ArrayList<Integer> list = tree.preOrder();
		System.out.println("PreOrder:"+ Arrays.toString(list.toArray()));
		verify(list, 1, 2, 3, 4, 5, 6, 7);
	}

}
