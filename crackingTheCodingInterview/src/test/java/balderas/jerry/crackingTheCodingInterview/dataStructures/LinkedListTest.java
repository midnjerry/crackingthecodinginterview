package balderas.jerry.crackingTheCodingInterview.dataStructures;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedList;
import balderas.jerry.crackingTheCodingInterview.dataStructures.Node;

public class LinkedListTest {
	private LinkedList list;

	@Before
	public void setUp() {
		list = new LinkedList();
	}

	
	public static void add(LinkedList list, int... numbers) {
		for (int num : numbers) {
			list.add(num);
		}
	}

	public static void verify(LinkedList list, int... numbers) {
		Node n = list.head;
		for (int num : numbers) {
			assertEquals(num, n.data);
			n = n.next;
		}
		assertNull(n);
	}
	
	@Test
	public void addAndGetNumber() {
		list.add(5);
		assertEquals(5, list.head.data);		
	}
	
	@Test
	public void addAndGetThreeNumbers() {
		list.add(5);
		list.add(6);
		list.add(7);
		Node n = list.head;
		assertEquals(5, n.data);
		n = n.next;
		assertEquals(6, n.data);
		n = n.next;
		assertEquals(7, n.data);
		n = n.next;
		assertNull(n);
	}
	
	@Test
	public void addThreeNumbersDeleteOneAndGetTwoNumbers() {
		list.add(5);
		list.add(6);
		list.add(7);
		list.delete(6);
		
		Node n = list.head;
		assertEquals(5, n.data);
		n = n.next;
		assertEquals(7, n.data);
		n = n.next;
		assertNull(n);
	}
	
	@Test
	public void ignoreDeletingNumberThatDoesntExist() {
		list.add(5);
		list.add(6);
		list.add(7);
		list.delete(8);
		
		Node n = list.head;
		assertEquals(5, n.data);
		n = n.next;
		assertEquals(6, n.data);
		n = n.next;
		assertEquals(7, n.data);
		n = n.next;
		assertNull(n);
	}
	
}
