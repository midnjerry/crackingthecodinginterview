package balderas.jerry.crackingTheCodingInterview.dataStructures;

public class LinkedList {

	public Node head;

	public void add(int data) {
		if (head == null) {
			head = new Node(data);
		} else {
			Node n = head;
			while (n.next != null) {
				n = n.next;
			}
			n.next = new Node(data);
		}
	}

	public void delete(int data) {
		Node n = head;
		if (n.data == data) {
			head = n.next;
			return;
		}
		while (n.next != null) {
			if (n.next.data == data) {
				n.next = n.next.next;
				return;
			}
			n = n.next;
		}
	}
}
