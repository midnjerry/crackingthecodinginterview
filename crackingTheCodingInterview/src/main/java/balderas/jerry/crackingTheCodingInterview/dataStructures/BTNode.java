package balderas.jerry.crackingTheCodingInterview.dataStructures;

public class BTNode {
	BTNode left;
	BTNode right;
	int data;

	public BTNode(int d) {
		data = d;
	}
}
