package balderas.jerry.crackingTheCodingInterview.dataStructures;

public class Node {

	public Node next;
	public int data;

	public Node(int d) {
		data = d;
		next = null;
	}
}
