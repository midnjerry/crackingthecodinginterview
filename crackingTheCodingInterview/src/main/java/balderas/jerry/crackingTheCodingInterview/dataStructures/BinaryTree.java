package balderas.jerry.crackingTheCodingInterview.dataStructures;

import java.util.ArrayList;

public class BinaryTree {
	private BTNode head;

	public void insertNode(int data) {
		if (head == null) {
			head = new BTNode(data);
		} else {
			BTNode n = head;

			while (true) {
				if (data > n.data) {
					if (n.right != null) {
						n = n.right;
					} else {
						n.right = new BTNode(data);
						break;
					}
				} else {
					if (n.left != null) {
						n = n.left;
					} else {
						n.left = new BTNode(data);
						break;
					}
				}
			}
		}
	}

	public ArrayList<Integer> preOrder() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		preOrder(head, result);
		return result;
	}

	private void preOrder(BTNode node, ArrayList<Integer> list) {
		if (node == null) {
			return;
		}
		list.add(node.data);		
		preOrder(node.left, list);
		preOrder(node.right, list);		
	}

	public ArrayList<Integer> postOrder() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		postOrder(head, result);
		return result;
	}

	private void postOrder(BTNode node, ArrayList<Integer> list) {
		if (node == null) {
			return;
		}
		postOrder(node.left, list);
		postOrder(node.right, list);
		list.add(node.data);
	}

	public ArrayList<Integer> inOrder() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		inOrder(head, result);
		return result;
	}

	public void inOrder(BTNode node, ArrayList<Integer> list) {
		if (node == null) {
			return;
		}
		inOrder(node.left, list);
		list.add(node.data);
		inOrder(node.right, list);
	}

}
