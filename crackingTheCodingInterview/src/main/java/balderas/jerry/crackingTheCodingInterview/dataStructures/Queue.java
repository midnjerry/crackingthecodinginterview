package balderas.jerry.crackingTheCodingInterview.dataStructures;

public class Queue {

	LinkedList list = new LinkedList();

	void enqueue(int data) {
		list.add(data);
	}

	Integer dequeue() {
		if (list.head == null)
			return null;

		Integer result = list.head.data;
		list.head = list.head.next;
		return result;		
	}
}
