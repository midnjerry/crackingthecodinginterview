package balderas.jerry.crackingTheCodingInterview.chapter1;

import java.util.HashSet;

public class Solution1_1 {

	/*
	 * Implement an algorithm to determine if a string has all unique characters.
	 */
	public boolean areLettersUnique(String string) {
		HashSet<Character> letters = new HashSet<Character>();

		if (string == null) {
			throw new NullPointerException();
		}

		for (int i = 0; i < string.length(); i++) {
			Character ch = Character.toUpperCase(string.charAt(i));
			if (Character.isAlphabetic(ch)) {
				if (letters.contains(ch)) {
					return false;
				} else {
					letters.add(ch);
				}
			}
		}

		return true;
	}

	/*
	 *  What if you can not use additional data structures?
	 */
	public boolean areLettersUniqueNoExtraDataStructures(String string) {
		String uppercase = string.toUpperCase();

		for (char ch = 'A'; ch <= 'Z'; ch++) {
			int count = getCount(ch, uppercase);
			if (count > 1) {
				return false;
			}
		}
		return true;
	}

	private int getCount(char ch, String string) {
		int count = 0;
		int index = 0;

		while (index != -1 && index < string.length()) {
			index = string.indexOf(ch, index);
			if (index > -1) {
				count++;
				index++;
			}
		}
		return count;
	}

}
