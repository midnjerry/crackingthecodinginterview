package balderas.jerry.crackingTheCodingInterview.chapter1;

public class Solution1_3 {

	public String removeDuplicateCharacters(String string) {
		if (string == null) {
			return null;
		}
		for (int i = 0; i < string.length(); i++) {
			char ch = string.charAt(i);
			
			string = string.substring(0, i + 1) + string.substring(i + 1).replaceAll(Character.toLowerCase(ch) + "", "")
					.replaceAll(Character.toUpperCase(ch) + "", "");
		}

		return string;
	}

}
