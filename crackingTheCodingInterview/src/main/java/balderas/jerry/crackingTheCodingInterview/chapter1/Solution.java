package balderas.jerry.crackingTheCodingInterview.chapter1;

import java.util.Arrays;
import java.util.HashSet;

public class Solution {

	public boolean isPermutationOf(String string, String string2) {
		char[] charArrayA = string.toCharArray();
		char[] charArrayB = string2.toCharArray();

		Arrays.sort(charArrayA);
		Arrays.sort(charArrayB);

		return String.valueOf(charArrayA).equals(String.valueOf(charArrayB));
	}

	public String URLify(String input) {
		StringBuilder builder = new StringBuilder();
		String[] words = input.split(" ");

		for (int i = 0; i < words.length; i++) {
			builder.append(words[i]);
			if (i != words.length - 1) {
				builder.append("%20");
			}
		}
		return builder.toString();
	}

	public boolean isPalindrome(String input) {
		if (input == null || input.isEmpty()) {
			return false;
		}

		HashSet<Character> charSet = new HashSet<Character>();

		for (int i = 0; i < input.length(); i++) {
			char ch = Character.toLowerCase(input.charAt(i));
			if (Character.isAlphabetic(ch)) {
				if (charSet.contains(ch)) {
					charSet.remove(ch);
				} else {
					charSet.add(ch);
				}
			}
		}

		return charSet.size() <= 1;
	}

	public boolean isOneChangeAway(String inputA, String inputB) {
		if (inputA.length() > inputB.length()) {
			return lookToRemoveLetter(inputA, inputB);
		} else if (inputA.length() == inputB.length()) {
			return lookToReplaceLetter(inputA, inputB);
		} else {
			return lookToRemoveLetter(inputB, inputA);
		}
	}

	private boolean lookToReplaceLetter(String inputA, String inputB) {
		int count = 0;
		int i = 0;
		while (count <= 1 && i < inputA.length()) {
			if (inputA.charAt(i) != inputB.charAt(i)) {
				count++;
			}
			i++;
		}
		return (count <= 1);
	}

	private boolean lookToRemoveLetter(String bigger, String smaller) {
		if (bigger.length() - smaller.length() > 1) {
			return false;
		}

		int count = 0;
		int i = 0;
		while (count <= 1 && i < smaller.length()) {
			if (bigger.charAt(i + count) != smaller.charAt(i)) {

				count++;
			} else {
				i++;
			}
		}
		return (count <= 1);
	}

	public String compress(String input) {
		StringBuilder builder = new StringBuilder();

		int count = 1;
		for (int i = 1; i < input.length(); i++) {
			char base = input.charAt(i - 1);
			if (base == input.charAt(i)) {
				count++;
			} else {
				builder.append(base + "" + count);
				count = 1;				
			}
			
			if (i == input.length()-1){
				builder.append(input.charAt(i) + "" + count);
			}
			
			if (builder.length() >= input.length())
				return input;
			
		}		
		return builder.toString();
	}

}
