package balderas.jerry.crackingTheCodingInterview.chapter1;

public class Solution1_2 {

	public Character[] getCString(String string) {
		if (string == null)
			return null;

		Character[] result = new Character[string.length() + 1];

		for (int i = 0; i < string.length(); i++) {
			result[i] = string.charAt(i);
		}
		result[string.length()] = '\0';
		return result;
	}

	public Character[] reverseCString(Character[] charArray) {
		if (charArray == null) {
			return null;
		}

		Character[] result = new Character[charArray.length];
		for (int i = 0; i < charArray.length - 1; i++) {
			result[i] = charArray[charArray.length - 2 - i];
		}
		
		result[result.length - 1] = '\0';
		return result;
	}

}
