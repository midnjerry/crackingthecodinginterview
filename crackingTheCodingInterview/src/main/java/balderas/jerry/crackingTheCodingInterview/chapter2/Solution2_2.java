package balderas.jerry.crackingTheCodingInterview.chapter2;

import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedList;
import balderas.jerry.crackingTheCodingInterview.dataStructures.Node;

public class Solution2_2 extends LinkedList {

	/*
	 * Implement an algorithm to find the nth to last element of a singly linked
	 * list
	 */
	public Integer getNthToLastElement(int n) {
		Node start = head;
		Node range = head;
		int count = 0;
		
		while (range != null && count < n){
			count++;
			range = range.next;
		}
		
		if (count != n){
			return null;
		}
		
		while (range.next != null) {
			range = range.next;
			start = start.next;
		}
		
		return start.data;
	}
}
