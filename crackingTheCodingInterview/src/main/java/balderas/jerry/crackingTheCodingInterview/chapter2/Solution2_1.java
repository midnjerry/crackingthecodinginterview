package balderas.jerry.crackingTheCodingInterview.chapter2;

import java.util.HashSet;

import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedList;
import balderas.jerry.crackingTheCodingInterview.dataStructures.Node;

public class Solution2_1 extends LinkedList {

	/*
	 * Write code to remove duplicates from an unsorted linked list.
	 */
	public void removeDuplicates() {
		if (head == null)
			return;

		Node n = head;
		HashSet<Integer> dataSet = new HashSet<Integer>();
		dataSet.add(n.data);

		while (n.next != null) {
			if (dataSet.contains(n.next.data)) {
				n.next = n.next.next;
			} else {
				dataSet.add(n.next.data);
				n = n.next;
			}
		}
	}

	/*
	 * How would you solve this problem if a temporary buffer is not allowed?
	 */
	public void removeDuplicates2() {
		if (head == null)
			return;

		Node start = head;

		while (start != null) {
			Node index = start;
			while (index.next != null) {
				if (start.data == index.next.data) {
					index.next = index.next.next;
				} else {
					index = index.next;
				}
			}
			start = start.next;
		}
	}

}
