package balderas.jerry.crackingTheCodingInterview.chapter2;

import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedList;
import balderas.jerry.crackingTheCodingInterview.dataStructures.Node;

public class Solution2_4 {

	/*
	 * Implement an algorithm to find the nth to last element of a singly linked
	 * list
	 */
	public LinkedList addOperation(LinkedList n, LinkedList m) {
		LinkedList result = new LinkedList();

		Node a = n.head;
		Node b = m.head;

		int carryover = 0;
		while (a != null || b != null || carryover > 0) {
			int sum = carryover;
			if (a != null) {
				sum += a.data;
				a = a.next;
			}
			if (b != null) {
				sum += b.data;
				b = b.next;
			}
			int digit = sum % 10;
			carryover = sum / 10;
			result.add(digit);
		}

		return result;
	}
}
