package balderas.jerry.crackingTheCodingInterview.chapter2;

import balderas.jerry.crackingTheCodingInterview.dataStructures.LinkedList;
import balderas.jerry.crackingTheCodingInterview.dataStructures.Node;

public class Solution2_3 extends LinkedList {

	/*
	 * Implement an algorithm to find the nth to last element of a singly linked
	 * list
	 */
	public void deleteNode(Node n) {
		if (head == null) {
			return;
		}

		Node index = head;
		if (index == n) {
			head = index.next;
			return;
		}

		while (index.next != null) {
			if (index.next == n) {
				index.next = n.next;
			} else {
				index = index.next;
			}
		}
	}
}
