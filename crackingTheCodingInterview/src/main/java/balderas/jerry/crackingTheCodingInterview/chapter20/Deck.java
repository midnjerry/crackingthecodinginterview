package balderas.jerry.crackingTheCodingInterview.chapter20;

import java.util.ArrayList;
import java.util.Random;

public class Deck {

	ArrayList<Card> deck;

	public Deck() {
		createDeck();
	}

	private void createDeck() {
		deck = new ArrayList<Card>();
		for (Suit suit : Suit.values()) {
			for (Value value : Value.values()) {
				deck.add(new Card(suit, value));
			}
		}
	}

	public int getSize() {
		return deck.size();
	}

	public Card drawCard() {
		return deck.remove(0);
	}

	public void shuffleDeck() {
		ArrayList<Card> shuffledDeck = new ArrayList<Card>();
		Random random = new Random();
		while (deck.size() > 0) {
			int index = random.nextInt(deck.size());
			Card card = deck.remove(index);
			shuffledDeck.add(card);
		}
		deck = shuffledDeck;
	}

	public class Card {
		private Suit suit;
		private Value value;

		public Card(Suit suit, Value value) {
			this.suit = suit;
			this.value = value;
		}

		public Suit getSuit() {
			return suit;
		}

		public Value getNumber() {
			return value;
		}

		public String toString() {
			return value.toString() + suit.toString();
		}

	}

	public enum Suit {
		HEARTS("\u2665"), SPADES("\u2664"), CLUBS("\u2663"), DIAMONDS("\u2666");

		private String mPrettyPrint;

		Suit(String prettyPrint) {
			mPrettyPrint = prettyPrint;
		}

		public String toString() {
			return mPrettyPrint;
		}
	}

	public enum Value {
		ACE("A"), TWO("2"), THREE("3"), FOUR("4"), FIVE("5"), SIX("6"), SEVEN("7"), EIGHT("8"), NINE("9"), TEN("10"),
		JACK("J"), QUEEN("Q"), KING("K");

		private String mPrettyPrint;

		Value(String prettyPrint) {
			mPrettyPrint = prettyPrint;
		}

		public String toString() {
			return mPrettyPrint;
		}
	}
}
