package balderas.jerry.crackingTheCodingInterview.chapter20;

import java.util.ArrayList;
import java.util.Random;

public class Solution {

	/*
	 * Write a function that adds two numbers. You should not use + or any
	 * arithmetic operators.
	 */
	public int add(int i, int j) {
		if (j == 0) {
			return i;
		}
		int xor = i ^ j;
		int carry = (i & j) << 1;
		return add(xor, carry);
	}

	/*
	 * Write a method to randomly generate a set of m integers from an array of
	 * size n. Each element must have equal probability of being chosen. Each
	 * element should only be selected twice. Do not use REMOVE for array.
	 */
	public int[] generateSet(int[] input, int m) {
		int[] result = new int[m];
		int[] duplicate = input.clone();
		Random random = new Random();
		for (int i = 0; i < m; i++) {
			int index = random.nextInt(input.length - i) + i;
			result[i] = duplicate[index];
			duplicate[index] = duplicate[i];
			duplicate[i] = result[i];
		}
		return result;
	}

	public int countTwos(int num) {
		if (num < 2) {
			return 0;
		} else if (num < 10) {
			return 1;
		}

		int n = num;
		int pow = 1;
		while (n > 9) {
			pow *= 10;
			n /= 10;
		}

		int digit = num / pow;
		int remainder = num % pow;

		if (digit != 2) {
			return countTwos(remainder) + countTwos(digit * pow - 1);
		} else {
			return remainder + 1 + countTwos(digit * pow - 1) + countTwos(remainder);
		}
	}

	public int bookSolution(int n) {
		// Base case
		if (n == 0)
			return 0;

		// 513 into 5 * 100 + 13. [Power = 100; First = 5; Remainder = 13]
		int power = 1;
		while (10 * power < n)
			power *= 10;
		int first = n / power;
		int remainder = n % power;

		// Counts 2s from first digit
		int nTwosFirst = 0;
		if (first > 2)
			nTwosFirst += power;
		else if (first == 2)
			nTwosFirst += remainder + 1;

		// Count 2s from all other digits
		int nTwosOther = first * bookSolution(power - 1) + bookSolution(remainder);

		return nTwosFirst + nTwosOther;
	}

}
