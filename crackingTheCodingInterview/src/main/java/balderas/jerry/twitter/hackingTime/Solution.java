package balderas.jerry.twitter.hackingTime;

public class Solution {

	public String encrypt(String input, String numericKey) {
		return algo(input, numericKey, true);
	}

	public String decrypt(String input, String numericKey) {
		return algo(input, numericKey, false);
	}

	private String algo(String input, String numericKey, boolean encryptionFlag) {
		int mult = -1;
		if (encryptionFlag) {
			mult = 1;
		}

		StringBuilder builder = new StringBuilder();

		for (int i = 0, j = 0; i < input.length(); i++) {
			char ch = input.charAt(i);
			if (Character.isAlphabetic(ch)) {
				int num = numericKey.charAt(j) - '0';
				builder.append(rollLetter(ch, mult * num));
				j++;
				if (j >= numericKey.length()) {
					j = 0;
				}
			} else {
				builder.append(ch);
			}
		}
		return builder.toString();
	}

	protected char rollLetter(char ch, int num) {
		char result = ch;
		if (Character.isLowerCase(ch)) {
			result = (char) (((ch - 'a' + num + 26) % 26) + 'a');
		} else {
			result = (char) (((ch - 'A' + num + 26) % 26) + 'A');
		}
		return result;
	}

	public String calcKey(String decoded, String encrypted) {
		if (decoded.length() != encrypted.length()) {
			return null;
		}

		StringBuilder builder = new StringBuilder();

		for (int i = 0; i < decoded.length(); i++) {
			char dch = decoded.charAt(i);
			char ech = encrypted.charAt(i);
			if (Character.isAlphabetic(dch)) {
				int digit = (((ech - dch + 26) % 26));
				builder.append(digit);
			}
		}
		return builder.toString();
	}

	public String printTuple(String input) {
		if (input.equals("()")) {
			return ("()");
		}
		StringBuilder result = new StringBuilder();		
		String[] elements = input.substring(1, input.length() - 1).split(",");
		long max = (long) 1 << elements.length;

		for (long i = 0; i < max; i++) {
			insertTuple(result, elements, i);
			addComma(result, i, max);
		}
		return result.toString();
	}

	private void addComma(StringBuilder result, long i, long max) {
		if (i < max - 1) {
			result.append(", ");
		}
	}

	private void insertTuple(StringBuilder result, String[] elements, long i) {
		result.append("(");
		for (int digit = 0; digit < elements.length; digit++) {
			if (getBit(i, elements.length - 1 - digit) == 1) {
				result.append(elements[digit]);
			} else {
				result.append("*");
			}
			addComma(result, digit, elements.length);			
		}
		result.append(")");
	}

	int getBit(long i, int kthBit) {
		return (int) ((i >> kthBit) & 1);
	}

}
